import express from 'express';
import pg from 'pg';

const server = express();

server.enable('trust proxy');

server.get('/', async (request, response) => {
  let visits;
  let errors;
  try {
    const client = new pg.Client({ connectionString: process.env.DATABASE_URL });
    await client.connect()
    await client.query('CREATE TABLE IF NOT EXISTS visits (instant bigint, ip text)');
    await client.query('INSERT INTO visits (instant, ip) VALUES ($1::bigint, $2::text)', [Date.now(), request.ip]);
    const res = await client.query('SELECT * FROM visits');
    visits = res.rows;
    await client.end();
  } catch (error) {
    errors = error;
  }

  // Local testing data
  if (process.env.PORT === undefined) {
    visits = [
      { instant: Date.now().toString(), ip: '99.111.90.72' },
      { instant: Date.now().toString(), ip: '99.211.9.15' },
    ];
  }

  response.send(`
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf8" />
    <title>Agenda</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script>
window.addEventListener('load', async _ => {
  for (const visitTr of document.querySelectorAll('tr.visit')) {
    const extremeIpLookupResponse = await fetch('https://extreme-ip-lookup.com/json/' + visitTr.dataset['ip']);
    const extremeIpLookupData = await extremeIpLookupResponse.json();

    const countryTd = document.createElement('td');
    countryTd.textContent = extremeIpLookupData.country;
    visitTr.appendChild(countryTd);

    const ispTd = document.createElement('td');
    ispTd.textContent = extremeIpLookupData.isp;
    visitTr.appendChild(ispTd);

    const restCountriesResponse = await fetch('https://restcountries.eu/rest/v2/alpha/' + extremeIpLookupData.countryCode);
    const restCountriesData = await restCountriesResponse.json();

    const flagTd = document.createElement('td');
    flagTd.innerHTML = '<img src="' + restCountriesData.flag + '" width="16" />';
    visitTr.appendChild(flagTd);
  }
});
    </script>
    <style>
body {
  font: icon;
}

tr {
  background: lightgray;
}
    </style>
  </head>
  <body>
    ${visits !== undefined ? `
    <table>
      <tr>
        <th>Instant</th>
        <th>Truncated IP</th>
        <th>Country</th>
        <th>Provider</th>
        <th>Flag</th>
      </tr>
      <!-- IP addresses by themselves are not personal data in this case (I don't think), but I still opt for truncated display -->
      ${visits.map(visit => `
        <tr class="visit" data-ip="${visit.ip}">
          <td>${new Date(Number(visit.instant)).toLocaleString()}</td>
          <td>…${visit.ip.slice(-8)}</td>
        </tr>
      `).join('')}
    </table>
    ` : ''}
    ${errors !== undefined ? errors.toString() : ''}
  </body>
</html>
  `);
});

server.listen(process.env.PORT || 3000, () => console.log('Listening…'));
